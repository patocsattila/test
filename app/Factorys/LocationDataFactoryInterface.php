<?php

namespace App\Factorys;

use App\Http\Requests\ApiRequest;

interface LocationDataFactoryInterface
{
    public function create(ApiRequest $request, int $locationId): Object;
}
