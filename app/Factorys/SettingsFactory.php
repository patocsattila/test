<?php

namespace App\Factorys;

use App\Http\Requests\ApiRequest;
use App\Models\Setting;

/**
 * Class SettingsFactory
 * @package App\Factorys
 */
class SettingsFactory implements LocationDataFactoryInterface
{
    /**
     * @param ApiRequest $request
     * @param int|null $locationId
     * @return Setting
     */
    public function create(ApiRequest $request, int $locationId): Setting
    {
        $data = [
            'name' => $request->get('name'),
            'value' => $request->get('value'),
            'description' => $request->get('description'),
            'location_id' => $locationId,
        ];

        return new Setting($data);
    }
}
