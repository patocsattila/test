<?php

namespace App\Services;

use Symfony\Component\Routing\Exception\InvalidParameterException;

class ValidatorService
{
    /**
     * @param object $validator
     * @param $request
     * @param $locationId
     * @return bool
     */
    public function validator(object $validator, $request, $locationId): bool
    {
        try {

            return $validator->validate($request, $locationId);

        } catch (InvalidParameterException $e) {

            throw new InvalidParameterException('Nincs kitöltve minden mező!');
        }
    }
}
