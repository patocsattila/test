<?php

namespace App\Services\Filter;

class SettingsFilterService
{
    /**
     * @param int $locationId
     * @return array
     */
    public function setFilter(int $locationId): array
    {
        $sortBy = 'name';
        $order = 'asc';

        $wheres = [];

        $wheres[] = ['location_id', "=", $locationId];

        return ['wheres' => $wheres, 'sortBy' => $sortBy, 'order' => $order, 'pagination' => ''];
    }
}
