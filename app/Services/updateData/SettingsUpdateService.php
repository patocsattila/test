<?php

namespace App\Services\updateData;

use App\Http\Requests\ApiRequest;
use App\Models\Setting;

class SettingsUpdateService
{
    /**
     * @param ApiRequest $request
     * @param Object $data
     * @param $locationId
     * @return Setting
     */
    public function updateData(ApiRequest $request, object $data, int $locationId): object
    {
        $data->name = $request->get('name');
        $data->value = $request->get('value');
        $data->description = $request->get('description');
        $data->location_id = $locationId;

        return $data;
    }
}
