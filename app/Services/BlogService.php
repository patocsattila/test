<?php

namespace App\Services;

use App\Http\Requests\BlogRequest;
use App\Repository\BlogRepository;
use Illuminate\Http\Request;

/**
 * Class BlogService
 * @package App\Service
 */
class BlogService
{
    /**
     * @var BlogRepository
     */
    private BlogRepository $repository;

    /**
     * BlogService constructor.
     * @param BlogRepository $repository
     */
    public function __construct(BlogRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return object|null
     */
    public function getAll(Request $request): ?object
    {
        $sortBy = 'id';
        $order = 'asc';
        if ($request->query->get('sortBy')) {
            $sortBy = $request->query->get('sortBy');
        }
        if ($request->query->get('order')) {
            $order = $request->query->get('order');
        }

        $pagination = $request->query->get('pagination', 0);

        $wheres = [];
        if ($request->query->get('filter')) {
            $filterFields = json_decode(base64_decode($request->query->get('filter')));

            foreach ($filterFields as $field => $value) {
                if ($value !== '') {
                    $equal = array('id');
                    if (in_array($field, $equal)) $wheres[] = array($field, "=", $value);

                    $like = array('name', 'post_code');
                    if (in_array($field, $like)) $wheres[] = array($field, "like", '%' . $value . '%');
                }
            }
        }

        return $this->repository->getAll($sortBy, $order, $wheres, $pagination);
    }

    /**
     * @param int $id
     * @return object|null
     */
    public function show(int $id): ?object
    {
        return $this->repository->show($id);
    }

    /**
     * @param BlogRequest $request
     * @return object|null
     */
    public function store(BlogRequest $request): ?object
    {
        return $this->repository->store($request);
    }

    /**
     * @param BlogRequest $request
     * @param int $id
     * @return object|null
     */
    public function update(BlogRequest $request, int $id): ?object
    {
        $blog = self::show($id);

        if ($blog) {
            return $this->repository->update($request, $blog);
        } else {
            return null;
        }
    }

    /**
     * @param int $id
     * @return int|null
     */
    public function destroy(int $id): ?int
    {
        $blog = self::show($id);

        if ($blog) {
            return $this->repository->destroy($blog);
        } else {
            return null;
        }
    }
}
