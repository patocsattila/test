<?php

namespace App\Services;

use App\Http\Requests\ApiRequest;

/**
 * Interface ServiceInterface
 * @package App\Service
 */
interface ServiceInterface
{
    public function getAction(int $locationId): array;

    public function showAction(int $id, int $locationId): array;

    public function store(ApiRequest $request, int $locationId): array;

    public function update(ApiRequest $request, int $id, int $locationId): array;
}
