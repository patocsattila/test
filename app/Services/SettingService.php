<?php

namespace App\Services;

use App\Http\Requests\ApiRequest;
use App\Models\Setting;
use App\Factorys\SettingsFactory;
use App\Repository\SettingRepository;
use App\Validators\SettingValidator;
use App\Services\updateData\SettingsUpdateService;
use App\Services\Filter\SettingsFilterService;

use Validator;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SettingService
 * @package App\Service
 */
class SettingService implements ServiceInterface
{
    /**
     * @var SettingRepository
     */
    private SettingRepository $repository;
    /**
     * @var SettingValidator
     */
    private SettingValidator $settingValidator;
    /**
     * @var SettingsFactory
     */
    private SettingsFactory $settingsFactory;
    /**
     * @var ValidatorService
     */
    private ValidatorService $validatorService;
    /**
     * @var SettingsUpdateService
     */
    private SettingsUpdateService $settingsUpdateService;
    /**
     * @var SettingsFilterService
     */
    private SettingsFilterService $settingsFilterService;

    /**
     * settingService constructor.
     * @param SettingRepository $repository
     * @param SettingValidator $settingValidator
     * @param SettingsFactory $settingsFactory
     * @param ValidatorService $validatorService
     * @param SettingsUpdateService $settingsUpdateService
     * @param SettingsFilterService $settingsFilterService
     */
    public function __construct(SettingRepository $repository, SettingValidator $settingValidator, SettingsFactory $settingsFactory, ValidatorService $validatorService,
                                SettingsUpdateService $settingsUpdateService, SettingsFilterService $settingsFilterService)
    {
        $this->repository = $repository;

        $this->settingValidator = $settingValidator;

        $this->settingsFactory = $settingsFactory;

        $this->validatorService = $validatorService;

        $this->settingsUpdateService = $settingsUpdateService;

        $this->settingsFilterService = $settingsFilterService;
    }

    /**
     * @param int $locationId
     * @return array
     */
    public function getAction(int $locationId): array
    {
        $filter = $this->settingsFilterService->setFilter($locationId);

        try {
            $settings = $this->repository->getAll($filter['sortBy'], $filter['order'], $filter['wheres'], $filter['pagination']);

            if ($settings === null) {
                return ['message' => 'Not found settings', 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }

        } catch (\Exception $e) {

            return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Get Setting List Successfully', 'data' => $settings, 'httpCode' => Response::HTTP_OK];
    }

    /**
     * @param int $id
     * @param int $locationId
     * @return array
     */
    public function showAction(int $id, int $locationId): array
    {
        try {
            $setting = $this->repository->getShow($id, $locationId);

            if ($setting === null) {
                return ['message' => 'Not found setting', 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }

        } catch (\Exception $e) {

            return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Get Setting List Successfully', 'data' => $setting, 'httpCode' => Response::HTTP_OK];
    }

    /**
     * @param ApiRequest $request
     * @param int $locationId
     * @return array
     */
    public function store(ApiRequest $request, int $locationId): array
    {
        $valid = $this->validatorService->validator($this->settingValidator, $request, $locationId);

        $result = null;

        try {

            if ($valid) {

                $setting = $this->settingsFactory->create($request, $locationId);


                $result = $valid ? $this->repository->persist($setting) : null;
            }

            if (!$result) {

                return ['message' => 'NULL VALUES ARE NOT ALLOWED', 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }

        } catch (\Exception $e) {

            return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Get Setting List Successfully', 'data' => $result, 'httpCode' => Response::HTTP_OK];
    }

    /**
     * @param ApiRequest $request
     * @param int $id
     * @param int $locationId
     * @return array
     */
    public function update(ApiRequest $request, int $id, int $locationId): array
    {
        $setting = $this->repository->getShow($id, $locationId);

        $valid = $this->validatorService->validator($this->settingValidator, $request, $locationId);

        $data = $setting[0] ?? null;

        if ($data) {

            try {

                $result = null;

                if ($valid) {

                    $data = $this->settingsUpdateService->updateData($request, $data, $locationId);

                    $result = $valid ? $this->repository->persist($data) : null;
                }

                if (!$result) {
                    return ['message' => 'Setting Updated Error', 'data' => $setting, 'httpCode' => Response::HTTP_OK];
                }

            } catch (\Exception $e) {
                return ['message' => $e->getMessage(), 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
            }

        } else {
            return ['message' => 'Error setting update', 'data' => [], 'httpCode' => Response::HTTP_NOT_ACCEPTABLE];
        }

        return ['message' => 'Setting Updated Successfully', 'data' => $result, 'httpCode' => Response::HTTP_OK];
    }
}
