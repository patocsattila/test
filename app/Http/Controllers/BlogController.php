<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogRequest;
use App\Models\Blog;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\BlogService;

/**
 * Class BlogController
 * @package App\Http\Controllers
 */
class BlogController extends Controller
{
    /**
     * @var BlogService
     */
    private BlogService $service;

    /**
     * BlogController constructor.
     * @param BlogService $service
     */
    public function __construct(BlogService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request): Response
    {
        return response($this->service->getAll($request));
    }

    /**
     * @param int $id
     * @return Application|ResponseFactory|Response
     */
    public function show(int $id): Response
    {
        return response($this->service->show($id));
    }

    /**
     * @param BlogRequest $request
     * @return Application|ResponseFactory|Response
     */
    public function store(BlogRequest $request): Response
    {
        $blog = $this->service->store($request);

        return response($blog);
    }

    /**
     * @param BlogRequest $request
     * @param int $id
     * @return Application|ResponseFactory|Response
     */
    public function update(BlogRequest $request, int $id): Response
    {
        $blog = $this->service->update($request, $id);

        return response($blog);
    }

    /**
     * @param int $id
     * @return Application|ResponseFactory|Response
     */
    public function destroy(int $id): Response
    {
        $blog = $this->service->destroy($id);

        return response($blog);
    }
}
