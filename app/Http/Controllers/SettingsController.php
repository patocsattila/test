<?php

namespace App\Http\Controllers;

use App\Http\Requests\SettingRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use App\Services\SettingService;

class SettingsController extends Controller
{
    /**
     * @var SettingService
     */
    private SettingService $service;

    public function __construct(SettingService $service)
    {
        $this->service = $service;
    }

    /**
     * @param int $locationId
     * @return mixed
     */
    public function index(int $locationId): Response
    {
        $result = $this->service->getAction($locationId);

        return response($result['data'], $result['httpCode']);
    }

    /**
     * @param int $id
     * @param int $locationId
     * @return Application|ResponseFactory|Response
     */
    public function show(int $id, int $locationId): Response
    {
        $result = $this->service->showAction($id, $locationId);

        return response($result['data'], $result['httpCode']);
    }

    /**
     * @param int $locationId
     * @param SettingRequest $request
     * @return Application|ResponseFactory|Response
     */
    public function store(int $locationId, SettingRequest $request): Response
    {
        $result = $this->service->store($request, $locationId);

        return response($result['data'], $result['httpCode']);
    }

    /**
     * @param SettingRequest $request
     * @param int $id
     * @return Application|ResponseFactory|Response
     */
    public function update(SettingRequest $request, int $id, int $locationId): Response
    {
        $result = $this->service->update($request, $id, $locationId);

        return response($result['data'], $result['httpCode']);
    }
}
