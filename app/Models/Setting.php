<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Setting
 * @package App\Models
 */
class Setting extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'schedule_settings';

    /**
     * @var string[]
     */
    protected $fillable = [
        'name', 'value', 'description', 'location_id'
    ];

    public $timestamps = false;
}
