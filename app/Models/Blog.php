<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Blog
 * @package App\Models
 */
class Blog extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'blog';

    /**
     * @var string[]
     */
    protected $fillable = [
        'name', 'text', 'date', 'user_id', 'cat_id'
    ];
}
