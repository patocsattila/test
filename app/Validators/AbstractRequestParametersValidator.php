<?php

namespace App\Validators;

use App\Http\Requests\ApiRequest;
use App\Http\Requests\SettingRequest;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Validator;

/**
 * Class RequestParametersValidator
 * @package App\Validator
 */
abstract class AbstractRequestParametersValidator implements RequestParametersInterface
{
    function validate(ApiRequest $request, $locationId): bool
    {
        $rules = $this->setConstraint();

        $data = $this->setData($request, $locationId);

        $validator = Validator::make(
            $data,
            $rules
        );

        return $this->returnData($validator);
    }

    protected function returnData(object $validator): bool
    {
        if ($validator->passes()) {
            return $validator->passes();

        } else {
            throw new InvalidParameterException('Nincs kitöltve minden mező!');
        }
    }

    protected function setConstraint(): array
    {
        $val = new SettingRequest();

        return $val->rules();
    }

    protected function setData(ApiRequest $request, int $locationId): array
    {
        return [
            'name' => $request->get('name'),
            'value' => $request->get('value'),
            'description' => $request->get('description'),
            'location_id' => $locationId,
        ];
    }
}
