<?php

namespace App\Validators;

use App\Http\Requests\ApiRequest;

/**
 * Interface RequestParametersInterface
 * @package App\Validator
 */
interface RequestParametersInterface
{
    /**
     * @param ApiRequest $request
     * @param $locationId
     * @return bool
     */
    function validate(ApiRequest $request, $locationId): bool;
}
