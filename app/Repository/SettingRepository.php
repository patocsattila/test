<?php

namespace App\Repository;

use App\Models\Setting;

/**
 * Class SettingRepository
 * @package App\Repository
 */
class SettingRepository extends AbstractRepository
{
    /**
     * @var Setting
     */
    private Setting $model;

    /**
     * SettingRepository constructor.
     * @param Setting $model
     */
    public function __construct(Setting $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $sortBy
     * @param string $order
     * @param array $wheres
     * @param string $pagination
     * @return array
     */
    public function getAll(string $sortBy, string $order, array $wheres, string $pagination): array
    {
        $settings = $this->model->orderBy($sortBy, $order);

        if (!empty($wheres)) {
            $settings = $settings->where($wheres);
        }

        $settings = ($pagination !== 'false') ? $settings->paginate($pagination)->all() : $settings->get()->all();;

        return $settings;
    }

    /**
     * @param int $id
     * @param int $locationId
     * @return array
     */
    public function getShow(int $id, int $locationId): array
    {
        $where = [['id', "=", $id], ['location_id', "=", $locationId]];

        return $this->model->query()->where($where)->get()->all();
    }

    /**
     * @param object $data
     * @return Setting
     * @throws \Exception
     */
    public function persist(object $data): object
    {
        return $this->save($data);
    }

    /**
     * @param object $setting
     * @return int|null
     */
    public function destroy(object $setting): int
    {
        return $setting->delete();
    }
}
