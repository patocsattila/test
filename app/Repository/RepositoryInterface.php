<?php

namespace App\Repository;

interface RepositoryInterface
{
    public function persist(object $data): object;

    public function remove(object $data): int;
}
