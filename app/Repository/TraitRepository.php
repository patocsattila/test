<?php

namespace App\Repository;

use Exception;

/**
 * Trait TraitRepository
 * @package App\Repository
 */
trait TraitRepository
{
    /**
     * @param object $data
     * @return int
     * @throws Exception
     */
    public function save(object $data): object
    {
        try {

            $data->save();

            return $data;

        } catch (Exception $e) {

            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param object $data
     * @return int
     * @throws Exception
     */
    public function destroy(object $data): int
    {
        try {

           $data->delete();

            return 1;

        } catch (Exception $e) {

            throw new Exception($e->getMessage());
        }
    }
}
