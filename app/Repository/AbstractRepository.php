<?php

namespace App\Repository;

use Exception;

abstract class AbstractRepository implements RepositoryInterface
{
    use TraitRepository;

    /**
     * @param object $data
     * @return object
     * @throws Exception
     */
    public function persist(object $data): object
    {
        return $this->save($data);
    }

    /**
     * @param object $data
     * @return int
     * @throws Exception
     */
    public function remove(object $data): int
    {
        return $this->destroy($data);
    }
}
