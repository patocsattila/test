<?php

namespace App\Repository;

use App\Http\Requests\BlogRequest;
use App\Models\Blog;

/**
 * Class BlogRepository
 * @package App\Repository
 */
class BlogRepository
{
    /**
     * @var Blog
     */
    private Blog $model;

    /**
     * BlogRepository constructor.
     * @param Blog $model
     */
    public function __construct(Blog $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $sortBy
     * @param string $order
     * @param array $wheres
     * @param int $pagination
     * @return object
     */
    public function getAll(string $sortBy, string $order, array $wheres, int $pagination): object
    {
        $blogs = $this->model->orderBy($sortBy, $order);

        if (!empty($wheres)) {
            $blogs = $blogs->where($wheres);
        }

        $blogs = ($pagination !== 'false') ? $blogs->paginate($pagination) : $blogs->get();

        return $blogs;
    }

    /**
     * @param $id
     * @return Blog
     */
    public function show($id): Blog
    {
        return $this->model->find($id);
    }

    /**
     * @param BlogRequest $request
     * @return object
     */
    public function store(BlogRequest $request): object
    {
        return $this->model->create([
            'name' => $request->get('name'),
            'date' => $request->get('date'),
            'text' => $request->get('text'),
            'user_id' => $request->get('user_id'),
            'cat_id' => $request->get('cat_id')
        ]);
    }

    /**
     * @param BlogRequest $request
     * @param $blog
     * @return object
     */
    public function update(BlogRequest $request, $blog): object
    {
        $blog->update([
            'name' => $request->get('name'),
            'date' => $request->get('date'),
            'text' => $request->get('text'),
            'user_id' => $request->get('user_id'),
            'cat_id' => $request->get('cat_id')
        ]);

        return $blog;
    }

    /**
     * @param Blog|null $blog
     * @return int|null
     */
    public function destroy(?Blog $blog): ?int
    {
        return $blog->delete();
    }
}
