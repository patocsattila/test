<?php

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="L5 OpenApi",
 *      description="L5 Swagger OpenApi description",
 *      @OA\Contact(
 *          email="darius@matulionis.lt"
 *      ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 */


use App\Http\Controllers\BlogController;
use App\Http\Controllers\SettingsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/blog', [BlogController::class, 'index']);
Route::get('/blog/show/{Blog}', [BlogController::class, 'show']);
Route::post('/blog/store', [BlogController::class, 'store']);
Route::put('/blog/update/{Blog}', [BlogController::class, 'update']);
Route::delete('/blog/destroy/{Blog}', [BlogController::class, 'destroy']);

Route::get('/settings/{LocationId}', [SettingsController::class, 'index'])->where('LocationId', '[0-9]+');
//Route::get('/settings/show/{Id}/{LocationId}', [SettingsController::class, 'show'])->where(['LocationId' => '[0-9]+', 'id' => '[0-9]+']);
Route::get('/settings/show/{Id}/{LocationId}', [SettingsController::class, 'show'])->where(['LocationId' => '[0-9]+', 'Id' => '[0-9]+']);
Route::post('/settings/add/{LocationId}', [SettingsController::class, 'store'])->where('LocationId', '[0-9]+');
Route::put('/settings/edit/{Id}/{LocationId}', [SettingsController::class, 'update'])->where(['Id' => '[0-9]+', 'LocationId' => '[0-9]+']);

Route::group(['middleware' => ['jwt.verify', 'jwt.refresh', 'cors']], function () {

});

\Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
    Log::info(json_encode($query->sql));
    Log::info(json_encode($query->bindings));
    //Log::info( json_encode($query->time) );
});
