<?php

namespace Tests\Unit\Services;

use App\Factorys\SettingsFactory;
use App\Http\Requests\SettingRequest;
use App\Models\Setting;
use App\Repository\SettingRepository;
use App\Services\updateData\SettingsUpdateService;
use App\Services\ValidatorService;
use App\Validators\SettingValidator;
use App\Services\Filter\SettingsFilterService;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Tests\TestCase;

class SettingsServiceTest extends TestCase
{
    private int $locationId;
    private int $id;
    private string $addData;
    private string $updateData;
    private string $addErrorData;
    private string $updateErrorData;

    private ?int $newId = null;

    private SettingRepository $settingRepository;
    /**
     * @var Setting
     */
    private Setting $model;
    /**
     * @var SettingValidator
     */
    private SettingValidator $settingValidator;
    /**
     * @var SettingsFactory
     */
    private SettingsFactory $settingsFactory;
    /**
     * @var ValidatorService
     */
    private ValidatorService $validatorService;
    /**
     * @var SettingsUpdateService
     */
    private SettingsUpdateService $settingsUpdateService;
    /**
     * @var SettingsFilterService
     */
    private SettingsFilterService $settingsFilterService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->locationId = 1;
        $this->id = 194;
        $this->addData = base64_encode(json_encode(["location_id" => "1", "value" => "1", "name" => 'teszt', "description" => "teszt szöveg"]));
        $this->addErrorData = base64_encode(json_encode(["location_id" => "1", "value" => "1", "name" => '', "description" => "teszt szöveg"]));
        $this->updateData = base64_encode(json_encode(["location_id" => "1", "value" => "1", "name" => 'teszt', "description" => "teszt szöveg 2"]));
        $this->updateErrorData = base64_encode(json_encode(["location_id" => "1", "value" => "1", "name" => '', "description" => "teszt szöveg 2"]));

        //$kernel = self::bootKernel();

        //$this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();

        //$this->documentManager = $kernel->getContainer()->get('doctrine_mongodb')->getManager();

        //$this->serviceFactory = new ServiceFactory();

        //$this->RequestParametersValidator = new RequestParametersValidator();
        $this->model = new Setting();

        $this->settingRepository = new SettingRepository($this->model);
        $this->settingValidator = new SettingValidator();
        $this->settingsFactory = new SettingsFactory();
        $this->validatorService = new ValidatorService();
        $this->settingsUpdateService = new SettingsUpdateService();
        $this->settingsFilterService = new SettingsFilterService();
    }

    /**
     * @group service-settings
     */
    public function testGetAction()
    {
        $settingsService = new \App\Services\SettingService($this->settingRepository, $this->settingValidator, $this->settingsFactory, $this->validatorService, $this->settingsUpdateService, $this->settingsFilterService);

        $result = $settingsService->getAction($this->locationId);

        $this->assertNotNull($result);
    }

    /**
     * @group service-settings
     */

    public function testIdAction(): void
    {
        $settingsService = new \App\Services\SettingService($this->settingRepository, $this->settingValidator, $this->settingsFactory, $this->validatorService, $this->settingsUpdateService, $this->settingsFilterService);

        $result = $settingsService->showAction($this->id, $this->locationId);

        $this->assertNotNull($result);
    }

    /**
     * @group service-settings
     */
    public function testAddSuccessAction(): void
    {
        $settingsService = new \App\Services\SettingService($this->settingRepository, $this->settingValidator, $this->settingsFactory, $this->validatorService, $this->settingsUpdateService, $this->settingsFilterService);

        $result = $settingsService->store(new SettingRequest(["location_id" => "1", "value" => "1", "name" => 'teszt', "description" => "teszt szöveg"]), $this->locationId);

        // $this->newId = $result['data']->getId();

        $this->assertNotNull($result);
    }

    /**
     * @group service-settings
     *
     */
    public function testAddErrorAction(): void
    {
        $this->expectException(InvalidParameterException::class);

        $settingsService = new \App\Services\SettingService($this->settingRepository, $this->settingValidator, $this->settingsFactory, $this->validatorService, $this->settingsUpdateService, $this->settingsFilterService);

        $result = $settingsService->store(new SettingRequest(["location_id" => "1", "value" => "1", "name" => '', "description" => "teszt szöveg"]), $this->locationId);

        $this->assertNotNull($result);
    }

    /**
     * @group service-settings
     */
    /*
    public function testUpdateAction(): void
    {
        $settingsService = new \App\Service\SettingService($this->settingRepository);

        $result = $settingsService->update(SettingRequest::class , $this->locationId);

        $this->assertNotNull($result);
    }
*/
    /**
     * @group service-settings
     */
    /*
    public function testUpdateErrorAction(): void
    {
        $this->expectException(InvalidParameterException::class);

        $settingsService = new SettingsService($this->entityManager, $this->serviceFactory, $this->RequestParametersValidator);

        $settingsService->updateAction($this->id, $this->locationId, $this->updateErrorData);
    }
    */
    /*
        protected function tearDown(): void
        {
            $em = $this->entityManager;

            $setting = $em->getRepository(Settings::class)->findOneBy(array('id' => $this->newId, 'locationId' => $this->locationId));

            if (!empty($setting)) {
                $em->remove($setting);
                $em->flush();
            }
        }*/
}
