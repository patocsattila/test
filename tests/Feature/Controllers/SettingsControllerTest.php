<?php

namespace Tests\Feature\Controllers;

use Tests\TestCase;

class SettingsControllerTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /*

    public function setUp(): void
    {
        //$this->client = static::createClient();
    }
    */

    /**
     * @test
     *
     * @group controller-settings
     */
    public function testGetAction()
    {
        $response = $this->get('/api/settings/' . $this->locationId);

        $response->assertStatus(200);
    }

    /**
     * @test
     *
     * @group controller-settings
     */
    public function idAction()
    {
        $response = $this->get('/api/settings/show/' . $this->id . '/' . $this->locationId);

        $response->assertStatus(200);
    }

    /**
     * @test
     *
     * @group controller-settings
     */
    public function addAction()
    {
        $response = $this->post('/api/settings/add/' . $this->locationId, ['name' => 'teszt 3 name', 'value' => 'teszt value', 'description' => 'teszt description', 'location_id' => '1']);
        //print_r($response->dump());

        $response->assertStatus(200);

    }

    /**
     * @test
     *
     * @group controller-settings
     */

    public function updateAction()
    {

        $response = $this->put('/api/settings/edit/' . $this->id . '/' . $this->locationId, ['name' => 'teszt5 name', 'value' => 'teszt value', 'description' => 'teszt description', 'location_id' => '1']);

        $response->assertStatus(200);
    }
}
